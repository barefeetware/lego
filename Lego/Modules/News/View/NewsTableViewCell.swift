//
//  NewsTableViewCell.swift
//
//  Created by Tom Brodhurst-Hill on 20/11/18.
//  Copyright © 2018 BareFeetWare. All rights reserved.
//

import UIKit
import BFWControls

class NewsTableViewCell: NibTableViewCell {

    @IBOutlet var detailImageView: UIImageView?
    
    @IBInspectable var detailImage: UIImage? {
        get {
            return detailImageView?.image
        }
        set {
            detailImageView?.image = newValue
        }
    }
    
}
