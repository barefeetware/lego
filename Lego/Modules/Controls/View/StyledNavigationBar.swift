//
//  StyledNavigationBar.swift
//  Lego
//
//  Created by Tom Brodhurst-Hill on 26/11/18.
//  Copyright © 2018 BareFeetWare. All rights reserved.
//

import UIKit

@IBDesignable class StyledNavigationBar: UINavigationBar {
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        commonInit()
    }
    
    required init?(coder: NSCoder) {
        super.init(coder: coder)
        commonInit()
    }
    
    func commonInit() {
        barTintColor = Style.Color.brandBackground.color
        tintColor = UIColor.white
        prefersLargeTitles = true
        barStyle = .black
    }
    
}
