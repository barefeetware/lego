//
//  Style.swift
//  Lego
//
//  Created by Tom Brodhurst-Hill on 26/11/18.
//  Copyright © 2018 BareFeetWare. All rights reserved.
//

import UIKit

enum Style {
    
    enum Color: String {
        case brandBackground
        
        var color: UIColor? {
            return UIColor(named: rawValue)
        }
    }
    
}
